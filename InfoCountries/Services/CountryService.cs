﻿using InfoCountries.Models;
using InfoCountries.ModelViews;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetRestCountries;

namespace InfoCountries.Services
{
    public sealed class CountryService : ICountryService
    {
        private readonly ApplicationDbContext _context;

        public CountryService(ApplicationDbContext context)
        {
            _context = context;
        }

        private async Task<Models.Country> GetCountryDB (string code)
        {
            var result = await _context.Countries.Where(x => x.Code == code).FirstOrDefaultAsync();
            return result;
        }
        
        private async Task<Region> GetRegionDb(string name)
        {
            var result = await _context.Regions.FirstOrDefaultAsync(x => x.Name == name);
            if (result == null)
            {
                Region region = new Region { Name = name };
                _context.Add(region);
                await _context.SaveChangesAsync();
                result = region;
            }
            return result;
        }

        private async Task<Town> GetCapital(string name)
        {
            var result = await _context.Towns.FirstOrDefaultAsync(x => x.Name == name);
            if (result == null)
            {
                Town capital = new Town { Name = name };
                _context.Add(capital);
                await _context.SaveChangesAsync();
                result = capital;
            }
            return result;
        }
        public async Task Create (CountryDto model)
        {
            var countryDb = await GetCountryDB(model.Code);

            var regionDb = await GetRegionDb(model.Region);

            var townDb = await GetCapital(model.Capital);           

            if (countryDb == null)
            {
                Models.Country country = new Models.Country
                {
                    Name = model.Name,
                    Area = Convert.ToDecimal(model.Area),
                    CapitalId = townDb.Id,
                    Code = model.Code,
                    Population = model.Population,
                    RegionId = regionDb.Id
                };
                _context.Add(country);
                await _context.SaveChangesAsync();
            }
            else
            {
                countryDb.CapitalId = townDb.Id;
                countryDb.RegionId = regionDb.Id;
            }
        }

        public async Task<CountryDto> Get(string Name)
        {
            var country = RestCountry.Name(Name).FirstOrDefault();            

            var countryDto = new CountryDto
            {
                Name = country.Name,
                Area = country.Area,
                Capital = country.Capital,
                Code = country.Alpha3Code,
                Population = country.Population,
                Region = country.Region
            };
            return countryDto;
        }

        public async Task<List<string>> GetCountries()
        {
            var countries = await _context.Countries.OrderBy(x=>x.Name).Select(x => x.Name).ToListAsync();
            return countries;
        }
    }
}
