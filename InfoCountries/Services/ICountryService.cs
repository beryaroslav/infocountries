﻿using InfoCountries.Models;
using InfoCountries.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoCountries.Services
{
    public interface ICountryService
    {
        Task Create(CountryDto model);
        Task<CountryDto> Get(string Name);

        Task<List<string>> GetCountries();
    }
}
