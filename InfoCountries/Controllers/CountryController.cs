﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InfoCountries.Models;
using DotNetRestCountries;
using Microsoft.EntityFrameworkCore;
using InfoCountries.ModelViews;
using InfoCountries.Services;

namespace InfoCountries.Controllers
{
    public class CountryController : Controller
    {
        private readonly ILogger<CountryController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly ICountryService countryService;
        public CountryController(ICountryService countryService, ILogger<CountryController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
            this.countryService = countryService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CountryNotFound()
        {
            return View();
        }
        /// <summary>
        /// Получить все страны
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetCountries()
        {            
            try
            {
                var countries = countryService.GetCountries().Result;
              
                if (countries != null && countries.Count > 0)
                {
                    return View("Index",countries);
                }
                else
                {
                    return RedirectToAction(nameof(CountryNotFound));
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);
            }
            return RedirectToAction(nameof(Index), "Home");
        }
        /// <summary>
        /// Получить информацию о стране
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Get(string Name)
        {            
            try
            {
                var country = countryService.Get(Name).Result;
                return View("getCountry", country);
            }            
            catch (Exception exception)
            {                
                _logger.LogError(exception, exception.Message);
                return RedirectToAction(nameof(CountryNotFound),"Country" ,Name);
            }
        }

        // GET: Warehouses/Create
        public IActionResult Create(string? Name)
        {
            try
            {
                var country = countryService.Get(Name).Result;
                return View(country);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, exception.Message);

            }
            return RedirectToAction(nameof(Index), "Home");
        }

        // POST: Warehouses/Create        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Code,Capital,Area,Population,Region")] CountryDto model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await countryService.Create(model);
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, exception.Message);
                    return View(model);
                }
                return RedirectToAction(nameof(Index),"Home");
            }
            return View(model);
        }
        
    
    }
}
