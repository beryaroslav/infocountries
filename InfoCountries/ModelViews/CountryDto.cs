﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoCountries.ModelViews
{
    public class CountryDto
    {
        //public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Capital { get; set; }

        public float Area { get; set; }

        public int Population { get; set; }

        public string Region { get; set; }
    }
}
