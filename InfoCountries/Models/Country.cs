﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoCountries.Models
{
    public class Country : BaseEntity
    {
        public string Code { get; set; }

        public int CapitalId { get; set; }

        public Town Capital { get; set; }

        public decimal Area { get; set; }

        public int Population { get; set; }

        public int RegionId { get; set; }

        public Region Region { get; set; }
    }
}
