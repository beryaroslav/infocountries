﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InfoCountries.Models
{
    public abstract class BaseEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(300, MinimumLength = 1)]
        public string Name { get; set; }
    }
}
