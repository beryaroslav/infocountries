﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace InfoCountries.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Town> Towns { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Region> Regions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
           

            modelBuilder.Entity<Country>().ToTable("Countries");
            modelBuilder.Entity<Region>().ToTable("Regions"); 
            modelBuilder.Entity<Town>().ToTable("Towns");

            
            base.OnModelCreating(modelBuilder);
        }
    }
}
