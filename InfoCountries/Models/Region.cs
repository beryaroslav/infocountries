﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoCountries.Models
{
    public class Region : BaseEntity
    {
        public ICollection<Country> Countries { get; set; }
    }
}
